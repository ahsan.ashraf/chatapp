package com.example.avanza.chatapp.Fragments;

import com.example.avanza.chatapp.Notification.MyResponse;
import com.example.avanza.chatapp.Notification.Sender;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by Avanza on 30-Nov-18.
 */

public interface APIService {
    @Headers(
            {
                    "Content-Type:application/json",
                    "Authorization:key=AAAAXkoanak:APA91bGrNj91sjq25vGZAuJYmMJP8tJQy-bpMnkhRLHcFxNAhy2eNsm3O5z0PgLb3gxTkTRC6AS9fwQ5AqH9quKylOQXg-cvaY7X7xyTjTpUW7CPl0TRE4Pgh3X6uJMahQ8zvqhAv9LP"
            }

    )

    @POST("fcm/send")
    Call<MyResponse> sendNotification(@Body Sender body);
}
