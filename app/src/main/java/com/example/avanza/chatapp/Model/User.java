package com.example.avanza.chatapp.Model;

/**
 * Created by Avanza on 11-Nov-18.
 */

public class User {
    private String userId;
    private String username;
    private String imageURL;
    private String status;

    public User(String id, String username, String imageURL, String status) {
        this.userId = id;
        this.username = username;
        this.imageURL = imageURL;
        this.status = status;
    }

    public User() {
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
