package com.example.avanza.chatapp.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.avanza.chatapp.MessageActivity;
import com.example.avanza.chatapp.Model.Chat;
import com.example.avanza.chatapp.Model.User;
import com.example.avanza.chatapp.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Avanza on 11-Nov-18.
 */

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.ViewHolder> {

    private ArrayList<User> users;
    private Context context;
    private boolean isChat;
    private String lastMessage;

    public UserAdapter(ArrayList<User> users, Context context, boolean isChat) {
        this.users = users;
        this.context = context;
        this.isChat = isChat;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.user_item, parent, false);
        return new UserAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final User user = users.get(position);
//        holder.imvProfile.setImageResource(R.mipmap.ic_launcher);
        if (user.getImageURL().equals("default"))
            holder.imvProfile.setImageResource(R.mipmap.ic_launcher);
        else
            Glide.with(context).load(user.getImageURL()).into(holder.imvProfile);

        holder.tvUsername.setText(user.getUsername());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, MessageActivity.class);

                intent.putExtra("userId", user.getUserId());
                context.startActivity(intent);
            }
        });

        if (isChat) {
            setLastMessage(user.getUserId(), holder.tvLastMsg);
        } else
            holder.tvLastMsg.setVisibility(View.GONE);

        if (isChat) {
            if (user.getStatus().equals("online")) {
                holder.statusOn.setVisibility(View.VISIBLE);
                holder.statusOff.setVisibility(View.GONE);
            } else {
                holder.statusOn.setVisibility(View.GONE);
                holder.statusOff.setVisibility(View.VISIBLE);
            }
        } else {
            holder.statusOn.setVisibility(View.GONE);
            holder.statusOff.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    private void setLastMessage(final String receiverId, final TextView tvLastMsg) {
        final FirebaseUser fbUser = FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Chats");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                lastMessage = "default";
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Chat chat = snapshot.getValue(Chat.class);
                    if (chat.getSender().equals(fbUser.getUid()) && chat.getReceiver().equals(receiverId) ||
                            chat.getSender().equals(receiverId) && chat.getReceiver().equals(fbUser.getUid()))
                        lastMessage = chat.getMessage();
                }
                switch (lastMessage) {
                    case "default":
                        tvLastMsg.setText("No Message");
                        break;
                    default:
                        tvLastMsg.setText(lastMessage);
                        break;
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView imvProfile, statusOn, statusOff;
        TextView tvUsername, tvLastMsg;
        View itemView;

        public ViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            imvProfile = itemView.findViewById(R.id.profile_image);
            tvUsername = itemView.findViewById(R.id.username);
            statusOff = itemView.findViewById(R.id.status_off);
            statusOn = itemView.findViewById(R.id.status_on);
            tvLastMsg = itemView.findViewById(R.id.last_msg);

        }
    }
}
