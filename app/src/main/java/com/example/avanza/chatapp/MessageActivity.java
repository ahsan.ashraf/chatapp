package com.example.avanza.chatapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.avanza.chatapp.Adapter.MessageAdapter;
import com.example.avanza.chatapp.Fragments.APIService;
import com.example.avanza.chatapp.Model.Chat;
import com.example.avanza.chatapp.Model.User;
import com.example.avanza.chatapp.Notification.Client;
import com.example.avanza.chatapp.Notification.Data;
import com.example.avanza.chatapp.Notification.MyResponse;
import com.example.avanza.chatapp.Notification.Sender;
import com.example.avanza.chatapp.Notification.Token;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MessageActivity extends AppCompatActivity {
    CircleImageView imvProfile;
    TextView tvTitle;
    EditText etMessage;
    ImageButton btnSend;
    Intent intent;
    DatabaseReference reference;
    FirebaseUser firebaseUser;
    RecyclerView recyclerView;
    MessageAdapter messageAdapter;
    ArrayList<Chat> chatList;
    Context context;
    ValueEventListener seenListner;
    String userid;
    APIService apiService;
    boolean notify = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);
        Toolbar tb = findViewById(R.id.toolbar);
        setSupportActionBar(tb);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        tb.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MessageActivity.this, MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
            }
        });
        context = this;
        intent = getIntent();
        userid = intent.getStringExtra("userId");

        imvProfile = findViewById(R.id.profile_image);
        tvTitle = findViewById(R.id.title);
        btnSend = findViewById(R.id.btn_send);
        etMessage = findViewById(R.id.et_message);
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etMessage.getText().toString().isEmpty())
                    Toast.makeText(getApplicationContext(), "You can not send empty message", Toast.LENGTH_SHORT).show();
                else {
                    sendMessage(firebaseUser.getUid(), userid, etMessage.getText().toString());
                }
                notify = true;
            }
        });
        apiService = Client.getInstance("https://fcm.googleapis.com/").create(APIService.class);

        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        linearLayoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(linearLayoutManager);
        chatList = new ArrayList<>();

        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        reference = FirebaseDatabase.getInstance().getReference("User").child(userid);
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                tvTitle.setText(user.getUsername());
                if (user.getImageURL().equals("default"))
                    imvProfile.setImageResource(R.mipmap.ic_launcher);
                else
                    Glide.with(getApplicationContext()).load(user.getImageURL()).into(imvProfile);
                readMessage(userid, user.getImageURL());

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        setSeenListner(userid);


    }

    private void setSeenListner(final String userId) {
        reference = FirebaseDatabase.getInstance().getReference("Chats");
        seenListner = reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Chat chat = snapshot.getValue(Chat.class);
                    if (chat.getSender().equals(userId) || chat.getReceiver().equals(firebaseUser.getUid())) {
                        HashMap<String, Object> hashMap = new HashMap<>();
                        hashMap.put("isseen", true);
                        snapshot.getRef().updateChildren(hashMap);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void readMessage(final String userId, final String imageURL) {
        reference = FirebaseDatabase.getInstance().getReference("Chats");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                chatList.clear();
                Chat chat;
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {

                    chat = snapshot.getValue(Chat.class);
                    if ((chat.getSender().equals(firebaseUser.getUid()) && chat.getReceiver().equals(userId)) ||
                            (chat.getSender().equals(userId) && chat.getReceiver().equals(firebaseUser.getUid())))
                        chatList.add(chat);
                }
                messageAdapter = new MessageAdapter(chatList, context, imageURL);
                recyclerView.setAdapter(messageAdapter);
//                messageAdapter.setChatList(chatList);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private void sendMessage(String sender, final String receiver, String message) {

        reference = FirebaseDatabase.getInstance().getReference("Chats");
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("sender", sender);
        hashMap.put("receiver", receiver);
        hashMap.put("message", message);
        hashMap.put("isseen", false);

        reference.push().setValue(hashMap);
        etMessage.setText("");

        final DatabaseReference chatRef = FirebaseDatabase.getInstance().getReference("Chatlist").
                child(firebaseUser.getUid()).child(userid);
        chatRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (!dataSnapshot.exists())
                    chatRef.child("id").setValue(userid);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        final String msg = message;
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("User").child(firebaseUser.getUid());
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                if (notify)
                    sendNotification(receiver, user.getUsername(), msg);
                notify = false;
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void sendNotification(final String receiver, final String username, final String msg) {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Token");
        Query query = reference.orderByKey().equalTo(receiver);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Token token = snapshot.getValue(Token.class);
                    Data data = new Data(firebaseUser.getUid(), R.mipmap.ic_launcher, username + ": " + msg, "New Message", userid);
                    Sender sender = new Sender(data, token.getToken());
                    apiService.sendNotification(sender).enqueue(new Callback<MyResponse>() {
                        @Override
                        public void onResponse(Call<MyResponse> call, Response<MyResponse> response) {
                            if (response.code() == 200) {
                                if (response.body().success != 1)
                                    Toast.makeText(MessageActivity.this, "Failed!", Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<MyResponse> call, Throwable t) {

                        }
                    });
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void setStatus(String status) {
        reference = FirebaseDatabase.getInstance().getReference("User").child(firebaseUser.getUid());
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("status", status);
        reference.updateChildren(hashMap);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setStatus("online");//.
        currentUser(userid);

    }

    @Override
    protected void onPause() {
        super.onPause();
        reference.removeEventListener(seenListner);

        setStatus("offline");
        currentUser("none");
    }

    private void currentUser(String userId) {
        SharedPreferences.Editor editor = getSharedPreferences("PREFS", MODE_PRIVATE).edit();
        editor.putString("currentUser", userId);
        editor.apply();

    }
}
