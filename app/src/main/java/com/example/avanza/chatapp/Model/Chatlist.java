package com.example.avanza.chatapp.Model;

/**
 * Created by Avanza on 26-Nov-18.
 */

public class Chatlist {
    private String id;

    public Chatlist(String id) {
        this.id = id;
    }

    public Chatlist() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
