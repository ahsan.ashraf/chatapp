package com.example.avanza.chatapp.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.avanza.chatapp.Model.Chat;
import com.example.avanza.chatapp.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Avanza on 12-Nov-18.
 */

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.ViewHolder> {
    private FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
    private ArrayList<Chat> chats;
    private Context context;
    private String imageURL;

    public MessageAdapter(ArrayList<Chat> chats, Context context, String imageURL) {
        this.chats = chats;
        this.context = context;
        this.imageURL = imageURL;
    }

    @NonNull
    @Override
    public MessageAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == 0) {
            View view = LayoutInflater.from(context).inflate(R.layout.chat_right, parent, false);
            return new MessageAdapter.ViewHolder(view);
        } else {
            View view = LayoutInflater.from(context).inflate(R.layout.chat_left, parent, false);
            return new MessageAdapter.ViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull final MessageAdapter.ViewHolder holder, int position) {
        Chat chat = chats.get(position);
        holder.tvMessage.setText(chat.getMessage());
        if (holder.imvProfile != null)
            if (imageURL.equals("default"))
                holder.imvProfile.setImageResource(R.mipmap.ic_launcher);
            else
                Glide.with(context).load(imageURL).into(holder.imvProfile);

        if (holder.tvSeen != null) {
            if (position == chats.size() - 1) {
                if (chat.isIsseen())
                    holder.tvSeen.setText("Seen");
                else
                    holder.tvSeen.setText("Delivered");

            } else
                holder.tvSeen.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return chats.size();
    }

    public void setChatList(ArrayList<Chat> chatList) {
        this.chats = chatList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        if (chats.get(position).getSender().equals(firebaseUser.getUid()))
            return 0;
        else
            return 1;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView imvProfile;
        TextView tvMessage, tvSeen;


        public ViewHolder(View itemView) {
            super(itemView);
            imvProfile = itemView.findViewById(R.id.profile_image);
            tvMessage = itemView.findViewById(R.id.message);
            tvSeen = itemView.findViewById(R.id.seen_status);

        }
    }
}

