package com.example.avanza.chatapp.Notification;

/**
 * Created by Avanza on 30-Nov-18.
 */

public class Token {
    private String token;

    public Token(String token) {
        this.token = token;
    }

    public Token() {
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
