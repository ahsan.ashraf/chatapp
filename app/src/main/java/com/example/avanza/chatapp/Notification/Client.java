package com.example.avanza.chatapp.Notification;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Avanza on 30-Nov-18.
 */

public class Client {
    private static Retrofit retrofit = null;

    public static Retrofit getInstance(String url) {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build();
        }
        return retrofit;
    }
}
